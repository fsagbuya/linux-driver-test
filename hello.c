#include <linux/module.h>
#include <linux/kernel.h>

int init_module(void)
{
    printk(KERN_INFO "Hello, world - this is the kernel speaking!\n");
    return 0;
}

void cleanup_module(void)
{
    printk(KERN_INFO "Goodbye, world - leaving the kernel!\n");
}

MODULE_LICENSE("GPL");